-module(shithead_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, start_child/2]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(SERVER, ?MODULE).
%% -define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
	supervisor:start_link({local, ?SERVER}, ?MODULE, []).
%%   gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).
%%   supervisor:start_link({local, ?MODULE}, ?MODULE, []).


start_child(Value, LeaseTime) ->
	io:fwrite("shithead_sup start_child~n"),
	supervisor:start_child(?SERVER, [Value, LeaseTime]).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
	io:fwrite("shithead_sup init function~n"),
	Server = {shithead_server, {shithead_server, start_link, []},
		permanent, 2000, worker, [shithead_server]},
	Children = [Server],
	RestartStrategy = {one_for_one, 0, 1},
	{ok, {RestartStrategy, Children}}.


