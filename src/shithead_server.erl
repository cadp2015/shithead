%%%-------------------------------------------------------------------
%%% @author halldor
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. maí 2015 21:52
%%%-------------------------------------------------------------------
-module(shithead_server).
-author("halldor").
-behaviour(gen_server).

%% API
-export([start_link/0, stop/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-define(SERVER, ?MODULE).

%% Starts the server.
start_link() ->
	gen_server:start_link({global, ?SERVER}, ?MODULE, [], []).

% Stops the server.
stop() ->
	gen_server:cast(?SERVER, stop).

% This is called when a connection is made to the server
init([]) ->
	io:fwrite("shithead_server init~n"),
	{ok, {0, no_game}}.

% handle_call is invoked in response to gen_server:call
handle_call(Request, From, {0, no_game}) ->
	io:fwrite("Registering new game~n"),
	Game_server = server:init_game_server({Request, element(1, From)}),
	{reply, {new_game, Game_server}, {1, Game_server}};
handle_call(Request, From, {3, Game_server}) -> %Game_server is state
	case server:register_player(Request, element(1, From), Game_server) of
		ok -> {reply, {join_game, Game_server}, {0, no_game}};
		dead ->
			New_Game_Server = server:init_game_server({Request, element(1, From)}),
			{reply, {join_game, New_Game_Server}, {1, New_Game_Server}}
	end;
handle_call(Request, From, {N, Game_server}) -> %Game_server is state
	case server:register_player(Request, element(1, From), Game_server) of
		ok -> {reply, {join_game, Game_server}, {N + 1, Game_server}};
		dead ->
			New_Game_Server = server:init_game_server({Request, element(1, From)}),
			{reply, {join_game, New_Game_Server}, {1, New_Game_Server}}
	end.


% handle_cast is invoked in response to gen_server:cast
handle_cast(_Msg, _State) ->
	io:fwrite("handle_cast~n"),
	{reply, _Msg}.

handle_info(_Msg, State) ->
	io:fwrite("handle_info~n"),
	{noreply, State}.

terminate(Reason, _State) ->
	error_logger:info_msg("Don't want to be terminated~p~n", [Reason]),
	ok.

code_change(_OldVersion, State, _Extra) -> {ok, State}.
