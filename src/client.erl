-module(client).
-export([play/1, brain/1]).
-include("include/shithead.hrl").

%% start playing a game on 'Server'
play(Server) -> feeder(Server).

%% channels user_input to Brain and follows instruction
feeder(Server) ->
	Brain = spawn_link(client, brain, [Server]), %% spawn logical unit
	io:fwrite("for help type \"help\"~n"),
	%% start feeding user input to Brain
	report_name_to_brain(Brain, get_name()),
	wait_for_swap_to_start(),
	feed_swap_information(Brain),
	wait_for_swap_confirmation(),
	feed_user_input(Brain),
	ok.

%% prompts the user for a username
get_name() ->
	Name = io:get_line("Name: "),
	if 
		Name /= "\n" -> string:strip(Name, right, $\n);
		true -> io:fwrite("Reserved: ~p~n", [Name]), get_name()
	end.

report_name_to_brain(Brain, Name) -> Brain ! {self(), user_input, Name}.

wait_for_swap_to_start() -> receive swap_start -> ok end.

feed_swap_information(Brain) -> 
	Swap = io:get_line("swap: "),
	case Swap of
		"\n" -> Brain ! {self(), done_swapping};
		"1,1\n" -> Brain ! {self(), swap, {1,1}}, feed_swap_information(Brain);
		"1,2\n" -> Brain ! {self(), swap, {1,2}}, feed_swap_information(Brain);
		"1,3\n" -> Brain ! {self(), swap, {1,3}}, feed_swap_information(Brain);
		"2,1\n" -> Brain ! {self(), swap, {2,1}}, feed_swap_information(Brain);
		"2,2\n" -> Brain ! {self(), swap, {2,2}}, feed_swap_information(Brain);
		"2,3\n" -> Brain ! {self(), swap, {2,3}}, feed_swap_information(Brain);
		"3,1\n" -> Brain ! {self(), swap, {3,1}}, feed_swap_information(Brain);
		"3,2\n" -> Brain ! {self(), swap, {3,2}}, feed_swap_information(Brain);
		"3,3\n" -> Brain ! {self(), swap, {3,3}}, feed_swap_information(Brain);
		"exit\n" -> exit("See you later");
		"help\n" -> io:fwrite("USAGE: [<hand>,<table>] <enter> (f.e. \"1,2\")~n", []),
			feed_swap_information(Brain);
		_Mistake -> 
			feed_swap_information(Brain)
	end.

wait_for_swap_confirmation() -> receive swap_stop -> ok end.

feed_user_input(Brain) -> 
	Message = io:get_line("input: "),
	case Message of
		"exit\n" -> exit("See you later");
		"help\n" -> 
			io:fwrite("USAGE: <number>|<action> <enter>~n"),
			io:fwrite("number: {1|2|...|N}, action: c (take a chance), t (take pile)~n"),
			feed_user_input(Brain);
		_Other -> ok
	end,
	send_user_input(Brain, Message),
	feed_user_input(Brain).

send_user_input(Brain, Message) -> Brain ! {user_input, Message}.

%% Brain activity, before play_loop, 
%% input: server address
brain(Server) ->
	io:fwrite("Connecting to server...~n", []),
	connect_to_server(Server),
	{Feeder, Name} = wait_for_parent_id_and_name(),
	io:format("Your username is: ~p.~n", [Name]),
	Game_server = register_user_to_game(Name),
	io:fwrite("Waiting for other players on server ~p~n", [Game_server]),
	{Player, Others, Deck} = wait_for_game_setup(),
	print_clear(),
	print_state(Player, Others, Deck, [], any),
	print_newline(),
	start_swapping(Feeder),
	Player_after_swap = swap_phase(Feeder, Player, Others, Deck),
	report_swap_information(Game_server, Player_after_swap),
	stop_swapping(Feeder),
	start_playing(Game_server, Player_after_swap),
	ok.

connect_to_server(Server) ->
	case net_adm:ping(Server) of
		pang ->
			exit("Could not establish connection to server@localhost");
		pong ->
			io:fwrite("Connection Established.~n", [])
	end.

wait_for_parent_id_and_name() -> receive {Feeder, user_input, Message} -> {Feeder, Message} end.

register_user_to_game(Name) -> {_, Game_server} = gen_server:call({global, shithead_server}, Name), Game_server.
	
wait_for_game_setup() -> receive {game_setup, {Player, Others}, Deck} -> {Player, Others, Deck} end.

start_swapping(Feeder) -> Feeder ! swap_start.

swap_phase(Feeder, Player, Others, Deck) ->
	receive 
		{Feeder, swap, {Card_on_hand, Face_up_card}} ->
			Player_after_swap = swap_cards(Player, Card_on_hand, Face_up_card),
			print_clear(), print_state(Player_after_swap, Others, Deck, [], any),
			swap_phase(Feeder, Player_after_swap, Others, Deck);
		{Feeder, done_swapping} ->
			Player
	end.

%% Swapping card number M on Hand with Face-up card N
%% returns updated player
swap_cards(Player,M,N) ->
	H_card = lists:nth(M, Player#player.hand),
	F_card = lists:nth(N, Player#player.face_up),
	Hand = [F_card|(Player#player.hand -- [H_card])],
	Face_up = [H_card|(Player#player.face_up -- [F_card])],
	Player#player{hand = lists:keysort(2, Hand), face_up = lists:keysort(2, Face_up)}.

report_swap_information(Game_server, Player) -> Game_server ! {swapped_cards, Player}, ok.

stop_swapping(Feeder) -> Feeder ! swap_stop.

%% Print initial state from server then start playing (play_loop)
start_playing(Game_server, Me) ->
	Lowest = wait_for_lowest_value(),
	Move = wait_for_initial_state(),
	io:fwrite("LOWEST CARD ", []), 
	print_value(Lowest), 
	print_newline(),
	play_loop(Game_server, Move, Me, now()).

wait_for_lowest_value() -> receive {lowest_value, Value} -> Value end.

wait_for_initial_state() ->
	receive
		{state, {Player, Others, Move, Turn, Deck, Pile}} -> 
			print_clear(),
			print_state(Player, Others, Deck, Pile, Turn)
	end, 
	Move.

play_loop(Game_server, Current_move, Me, Time) ->
	receive 
		{state, {Player, Others, Move, Turn, Deck, Pile}} -> 
			print_clear(),
			print_state(Player, Others, Deck, Pile, Turn),
			play_loop(Game_server, Move, Player, now());
		{user_input, Message} ->
			case Message of
				"\n" -> ok;
				"c\n" -> send_move(Game_server, Current_move, Time, chance);
				"t\n" -> send_move(Game_server, Current_move, Time, take_pile);
				_Normal -> 
					Card = parse_input_to_card(Message, Me),
					if 
						Card == illegal -> 
							io:fwrite("No Card number ~p~n", [string:strip(Message, right, $\n)]);
						true -> 
							send_move(Game_server, Current_move, Time, Card)
					end
			end,
			play_loop(Game_server, Current_move, Me, Time);
		{winner, Winner} ->
			if 
			    Winner#player.pid == self() -> io:fwrite("Congratulations, you won the game !!!~n", []);
				true -> io:fwrite("Sorry... you lost to ~p~n", [Winner#player.name])
			end
	end.

send_move(Game_server, Current_move, Time, Action) ->
	Game_server ! {self(), {play, Current_move, timer:now_diff(now(), Time), Action}}, ok.

parse_input_to_card(Message, Me) ->
	Index = element(1, string:to_integer(Message)),
	if 
		is_integer(Index) -> find_card(Me, Index);
		true -> illegal
	end.

%% Find the card to send to Game if exists
find_card(Player, Index) when Index > 0 ->
	find_card(Player#player.hand, Player#player.face_up, Player#player.face_down, Index);
find_card(_, _) -> illegal.

%% Send card from face_down cards
find_card([],[], Face_down, Index) when length(Face_down) >= Index -> lists:nth(Index, Face_down);
%% Send card from face_up cards
find_card([],Face_up, _ , Index) when length(Face_up) >= Index -> lists:nth(Index, Face_up);
%% Send card from hand
find_card(Hand, _ , _ , Index) when length(Hand) >= Index -> lists:nth(Index, Hand);
find_card(_, _ , _ , _Index) -> illegal.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

print_clear() ->
	%% Voodoo, clear terminal screen (unix) and
	%% move output to coordinates {0,0}, (top left corner).
	io:format("\033[2J"), io:format("\e[~B;~BH", [0,0]).

print_state(Player, Others, Deck, Pile, Turn) ->
	Others_count = length(Others),
	Default_width = 18,
	if
		Others_count == 0 -> Indent = 4, Width = Default_width;
		true -> 
			Indent = (Others_count*17 - 12) div 2,
			Width = Default_width + 17*(Others_count-1)
	end,
	print_others(Others, Turn),
	print_margins(2, Width),
	print_delim(), print_ws(Indent), print_card(Pile), print_ws(1), print_card(Deck), print_ws(Width - Indent - 13), print_delim(),	print_newline(),
	print_margins(1, Width),
	print_player(Player, Others, Turn),
	print_newline().

print_delim() -> io:fwrite("|").

print_margins(0, _) -> ok;
print_margins(N, Width) ->
	print_margin(Width),
	print_margins(N-1, Width).
print_margin(N) ->
	io:fwrite("|"),	print_ws(N-2), io:fwrite("|"), print_newline().

print_indecies(Player) ->
	Number_of_cards = {length(Player#player.hand), length(Player#player.face_up), length(Player#player.face_down)},
	case Number_of_cards of
		{0, 0, Down} -> print_index(Down, 1);
		{0, Up, _} -> print_index(Up, 1);
		{Hand, _, _} -> print_index(Hand, 1)
	end.

print_index(0,_) -> ok;
print_index(M,N) when N > 9 -> 
	io:fwrite(" ~p  ", [N]), print_index(M-1, N+1);
print_index(M,N) -> 
	io:fwrite(" ~p   ", [N]), print_index(M-1, N+1).

print_ws(0) -> ok;
print_ws(N) -> io:fwrite(" "), print_ws(N-1).

print_player(Player, Others, Turn) ->
	Others_count = length(Others),
	Default_width = 18,
	if
		Others_count == 0 -> Indent = 1, Edges = 16, Width = Default_width;
		true -> 
			Indent = round((Others_count*17-16) / 2),
			Edges = Others_count * 17 - 1,
			Width = Default_width + 17*(Others_count-1)
	end,
	Indent_after = Width - Indent - 17,
	if
		Turn == Player#player.pid -> 
			print_delim(), io:fwrite(" IT'S YOUR TURN!"), print_ws(Width - 18), print_delim(), print_newline();
		true -> print_margin(Width)
	end,
	print_delim(), print_ws(Indent),
	if 
		length(Player#player.face_up) == 0 ->
			print_cards(Player#player.face_down, 0, down);
		true ->
			print_cards(Player#player.face_up, 0, up)
	end,
	print_ws(Indent_after),	print_delim(), print_newline(),
	print_delim(), print_edge(Edges), print_delim(), print_newline(),
	print_delim(), print_ws(Indent), print_indecies(Player), print_ws(Indent_after), print_delim(), print_newline(),
	print_delim(), print_ws(Indent), print_hand(Player#player.hand), print_ws(Indent_after), print_delim(), print_newline().

print_others(Others, Turn) ->
	print_names(Others, Turn), print_newline(),
	print_hands(Others), print_newline(),
	print_edges(Others), print_newline(),
	print_table_cards(Others), print_newline().

print_table_cards([]) -> print_delim();
print_table_cards([Player|Players]) ->
	print_delim(), print_ws(1),
	if 
		length(Player#player.face_up) == 0 ->
			print_cards(Player#player.face_down, 0, down);
		true ->
			print_cards(Player#player.face_up, 0, up)
	end,
	print_table_cards(Players).

print_hands([]) -> print_delim();
print_hands([Player|Players]) ->
	Count = length(Player#player.hand),
	if 
		Count > 9 ->
			print_delim(), print_ws(3);
		true ->
			print_delim(), print_ws(4)
	end,
	io:fwrite("~p Cards     ", [Count]),
	print_hands(Players).

print_edges([]) -> print_delim();
print_edges([_|Xs]) ->
	print_delim(), print_edge(16),
	print_edges(Xs).

print_names([], _) -> ok;
print_names(Players, Turn) -> print_delim(), print_ws(1), print_names(Players, Turn, {1,0}).
print_names([], _, _) -> ok;
print_names([Player|Players], Turn, {X,Y}) ->
	Offset = 17,
	print_name(Player, Turn), 
	io:format("\e[~B;~BH", [Y,X+Offset]), 
	print_delim(), print_ws(1),
	print_names(Players, Turn, {X+Offset,Y}).

print_edge(0) -> ok;
print_edge(N) -> io:fwrite("-", []), print_edge(N-1).

print_newline() -> io:fwrite("~n", []).

print_name(Player, Turn) -> 
	io:fwrite("~p:", [Player#player.name]),
	if
		Turn == Player#player.pid -> io:fwrite(" *", []);
		true -> ok
	end.

print_hand([]) -> ok;
print_hand([Card|Cards]) -> print_card(Card), 
	print_hand(Cards).

%% Print at 3, cardback/cardfront of face_up or face_down
print_cards(_, 3, _) -> ok;
print_cards([Card|Cards], N, Visible) -> 
	case Visible of
		up -> print_card(Card);
		down -> print_card(true)
	end,
	print_cards(Cards, N+1, Visible);
print_cards([],N, Visible) ->
	case Visible of
		up -> print_card(true);
		down -> print_card(false)
	end,
	print_cards([], N+1, Visible).

print_card({deck, Remaining}) when Remaining > 9 -> print_delim(), io:fwrite("~p", [Remaining]), print_delim(), print_ws(1);
print_card({deck, Remaining}) -> print_delim(), print_ws(1), io:fwrite("~p", [Remaining]), print_delim(), print_ws(1);
print_card(#card{value = Value, suit = Suit}) -> print_delim(), print_value(Value), io:format([Suit]), print_delim(), print_ws(1);
print_card(true) -> print_delim(), io:fwrite("##", []), print_delim(), print_ws(1);
print_card(_) -> print_delim(), io:fwrite("XX", []), print_delim(), print_ws(1).

print_value(Value) ->
	case Value of
		10 -> io:fwrite("T");
		11 -> io:fwrite("J");
		12 -> io:fwrite("Q");
		13 -> io:fwrite("K");
		14 -> io:fwrite("A");
		Normal -> io:fwrite("~p", [Normal])
	end.
