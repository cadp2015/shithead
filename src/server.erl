%%%-------------------------------------------------------------------
%%% @author halldor
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 10. maí 2015 12:09
%%%-------------------------------------------------------------------
-module(server).
-author("halldor").

-export([init_game_server/1, register_player/3, start_server/1]).

%% API
-include("include/shithead.hrl").

%% Functions

% --------------------------------------------------------------------------------------
% This block concerns initializing a game and waiting for players
% --------------------------------------------------------------------------------------

%% Creates a new game with inital player.
init_game_server(First_Player) ->
	spawn(server, start_server, [First_Player]).

%% Start a server.
start_server({Name, Player_Pid}) ->
	io:fwrite("Starting new game~n"),
	Players = await_players(3, [#player{pid = Player_Pid, name = Name}]),
	start_game(Players).

%% Create a player in the game Server_pid
register_player(Name, Player_pid, Server_pid) ->
	Server_pid ! {self(), {join, #player{pid = Player_pid, name = Name}}},
	receive
		ok -> ok
	after 100 -> dead
	end.

%% Awaits for players to connect.
%% When four players have connect 20 sek. has passed, the games starts
%% with the players connected.
%% gen_server instance will call this function to connect players that
%% tries to connect.
await_players(0, Players) -> Players;
await_players(N, Players) ->
	receive
		{From, {join, Player}} ->
			io:fwrite("Player joined ~p, pid ~p~n", [Player#player.name, Player#player.pid]),
			From ! ok,
			await_players(N - 1, [Player | Players])
	after 20000 -> Players
	end.
% --------------------------------------------------------------------------------------


% --------------------------------------------------------------------------------------
% This block concerns dealing cards and finding lowest card in initial state
% --------------------------------------------------------------------------------------

%% Creates a deck of shuffled cards.
create_deck() ->
	Values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],

	%% Those numbers reprisents unicode carahters for each suit.
	Suit = [9829, 9827, 9830, 9824],
	Deck = [#card{value = V, suit = S} || V <- Values, S <- Suit],
	shuffle(Deck).

deal_players([], Deck, Updated_players) -> {Updated_players, Deck};
deal_players([Player | Players], Deck, Updated_players) ->
	{Hand, Face_up, Face_down, Deck_after} = deal_hand(Deck),
	deal_players(Players, Deck_after, 
				 [Player#player{hand = lists:keysort(2, Hand), face_up = lists:keysort(2, Face_up), face_down = Face_down} | Updated_players]).

%% Takes three times three cards from the deck.
deal_hand([C1, C2, C3, C4, C5, C6, C7, C8, C9 | Deck]) ->
	{[C1, C2, C3], [C4, C5, C6], [C7, C8, C9], Deck}.


%% Function borrowed from http://www.codecodex.com/wiki/Shuffle_an_array, 2015-05-10
shuffle(List) ->
	random:seed(now()),
	Random_list = [{random:uniform(), X} || X <- List],
	[X || {_, X} <- lists:sort(Random_list)].

find_lowest_card([], Lowest) -> Lowest;
find_lowest_card([Player | Players], Lowest) ->
	io:fwrite("Hand ~p~n", [Player#player.hand]),
	Min = lists:min((lists:map(fun value_card/1, Player#player.hand))),
	io:fwrite("Lowest ~p~n", [Min]),
	if
		Min < Lowest ->
			find_lowest_card(Players, Min);
		Min >= Lowest ->
			find_lowest_card(Players, Lowest)
	end.

find_lowest_card(Players) ->
	find_lowest_card(Players, 14).

% Helper function to find lowest card but exclude special cards
value_card(Card) ->
	Value = Card#card.value,
	case lists:member(Value, [2, 5, 10]) of
		true -> 14;
		false -> Value
	end.
% --------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------
% This block concerns waiting untill all players have compleated the swap round
% --------------------------------------------------------------------------------------

swap_round(Game_State) ->
	io:fwrite("Swapping round begins~n"),
	await_players_swapping(Game_State#game.noplayer, []).

await_players_swapping(0, Players) -> Players;
await_players_swapping(N, Players) ->
	receive
		{swapped_cards, Player} ->
			await_players_swapping(N-1, [Player | Players])
	end.
% --------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------
% This block concerns the main game play 
% --------------------------------------------------------------------------------------

%% Starts the game with players.
start_game(Players) ->
	io:fwrite("Game has started~n"),
	Deck = create_deck(),
	{Players_ready, Updated_deck} = deal_players(Players, Deck, []),
	notify_initial_hand(Players_ready, Players_ready, Updated_deck),

	% Create the game state of the game.
	Game_State = #game{ players = Players_ready, deck = Updated_deck, noplayer = length(Players_ready)},
	Players_swapped = swap_round(Game_State),
	game_loop_init(Game_State#game{players = Players_swapped}).


%% First turn in the game.
game_loop_init(Game_State) ->
	% Report the lowes value on any hand, the player with the lowes value begins. Only the lowest value
	% can be played so we verify that.
	Lowest = find_lowest_card(Game_State#game.players),
	notify_lowest_value(Game_State#game.players, Lowest),
	notify_state(Game_State),

	%% Await for the inital play.
	receive
		%% Initial play received, can be from anyone that has the lowest card, and can only be the lowest card.
		{Pid, {play, 0, _Time, #card{value = V}=Card}} when V == Lowest ->
			Fixed_Players = fix_queue(Game_State#game.players, Pid, Game_State#game.noplayer),
			Updated_Game_State = game_logic(Game_State#game{players = Fixed_Players}, Card),
			game_loop(Updated_Game_State#game{move = 1});

		%% All other moves are discared.
		All -> 
			io:fwrite("In the trash can GameLoopInit with: ~p~n",[All]),
			game_loop_init(Game_State)
	end.

%% The main game loop of the game.
%% First is the the state of the game broadcast to all players.
%% Then wait for any message, from them and respond.
%% Move counter holds the current move, so we can discard older moves
%% that may have come too late to process.
game_loop(#game{players = [Player|_]}=Game_State) ->
	notify_state(Game_State),

	%% Values for pattermatching.
	Turn = Player#player.pid,
	Pile_value = pile_value(Game_State),
	Game_Move = Game_State#game.move,

	%% When the first message arives, wait for 100 ms, for other messages that may have been played befor that message.
	%% Messages have time diff, that is how long the user was reacting to the game update. This value is used
	%% to detrerman which message will get to be played.
	Message = receive
		          {_, {play, Move1, _, _}}=M when Game_Move == Move1 -> timer:sleep(100), await_other_messages(M);
				  All1 -> io:fwrite("Got some message, ~p~n", [All1]), game_loop(Game_State)
		      end,

	%% After prioritizing which message will be process, then we process that message.
	case Message of
		%% Shooting in a card. Player that doesn't have the turn plays same value card an steals the turn.
		{Pid, {play, Move, _, #card{value = Value} = Card}} when Move==Game_Move, Value == Pile_value, Turn /= Pid ->
			% Update the player order.
			[Fixed_Player | Fixed_Players] = fix_queue(Game_State#game.players, Pid, Game_State#game.noplayer),

			% User cannot shoot in a card that is face down.
			if 
				Fixed_Player#player.hand == [] andalso Fixed_Player#player.face_up == [] -> game_loop(Game_State);
				true -> ok
			end,
			%% Process the gameplay and repeat the loop.
			Updated_Game_State = game_logic(Game_State#game{players = [Fixed_Player | Fixed_Players]}, Card),
			game_loop(Updated_Game_State#game{move = Game_Move + 1});

		%% Normal game play. Process the play.
		{Pid, {play, Move, _Time, Card}} when Turn == Pid, Move == Game_Move ->
			case game_logic(Game_State, Card) of
				{illegal, Msg} ->
					io:fwrite("Illegal argument: ~p~n",[Msg]),
					game_loop(Game_State);
				New_Game_State ->
					case has_winner(New_Game_State) of
						{yes, Winner} ->
							notify_state(New_Game_State),
							notify_winner(Winner, New_Game_State);
						no -> game_loop(New_Game_State#game{move = Game_Move + 1})
					end
			end;
		%% Discard message that should not have been sent.
		All ->
			io:fwrite("In the trash can with: ~p, Move: ~p, Turn: ~p~n",
				[All, Game_State#game.move, Turn]),
			game_loop(Game_State)
	end.

%% Check to see if other players have sent a move that should have been process instead of the former one.
await_other_messages({_, {_, Move, Old_Time, _}} = Old_Message) ->
	receive
		%% Use this message instead of the older one.
		{_, {play, Move, Time, _}} = Other_Message when Old_Time > Time ->
			await_other_messages(Other_Message);
		%% Discard messages that are newer.
		Invalid -> io:fwrite("Message discared ~p~n", [Invalid]), await_other_messages(Old_Message)
	after 0 -> Old_Message
	end.

% Helper function to get value of the card on top of the pile
pile_value(#game{pile = []}) -> 0;
pile_value(#game{pile = [Card|_]}) ->
	Card#card.value.
% --------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------
% This block concerns the game logic and what actions to take
% --------------------------------------------------------------------------------------

%% Game logic process the play based on command.
game_logic(#game{deck = []}, chance) ->
	{illegal, "No chance on empty deck"};
%% Player takes a chance when the pile is empty. Allowed, card from the deck is put on the pile.
game_logic(#game{players = Players,deck = [Top | Deck], pile = []} = Game_State, chance) ->
	Game_State#game{deck = Deck, pile = [Top], players = change_turn(Players)};
%% Player takes a chance, Card is put on the pile from the deck and action as taken based on that.
game_logic(#game{players = Players, deck = [Deck_Top | Deck]} = Game_State, chance) ->
	Pile = add_to_Pile(Game_State, Deck_Top),
	do_action(Players, Game_State#game{pile = Pile, deck = Deck});
%% Player wants to take the pile. Allowed.
game_logic(Game_State, take_pile) ->
	do_take_pile(Game_State);
%% Updates the game state based on the card played.
game_logic(#game{players = [Player|Players]} = Game_State, Card) ->
	Updated_Player = remove_card_from_player(Player, Card),
	Pile = add_to_Pile(Game_State, Card),
	do_action([Updated_Player|Players], Game_State#game{pile = Pile}).

%% Perform action based on the pile.
%% Three things can happen when a card is placed on the pile.
%% * The pile is discarded and the player gets another turn.
%% * The player needs to take the pile to its hand.
%% * The card is placed on the pile.
%% The cards on hand and face up cards are sorted, based on value.
do_action([Player|Players], #game{pile = Pile}=Game_State) ->
	{P, State, Change_Turn} = case action_to_take(Pile) of
		                          discard_pile ->
			                          {Player, Game_State#game{pile = []}, false};
		                          take_pile ->
			                          Hand = lists:keysort(2, Player#player.hand++Pile),
			                          Updated_Player = Player#player{hand = Hand},
			                          {Updated_Player, Game_State#game{pile = []}, true};
		                          normal ->
			                          {Player, Game_State#game{pile = Pile}, true}
	                          end,
	{P2, New_State} = update_hand(P, State),
	%% do change turn if action calls for it.
	if
		Change_Turn == true ->  New_State#game{players = change_turn([P2|Players])};
		Change_Turn == false -> New_State#game{players = [P2|Players]}
	end.


%% Perform the take pile action. Adds the pile to players hand.
do_take_pile(#game{players = [Player | Players], pile = Pile} = Game_State) ->
	Hand = lists:keysort(2, Player#player.hand++Pile),
	P = Player#player{hand = Hand},
	Game_State#game{players = change_turn([P | Players]), pile = []}.


%% After a player plays a card, new one is drawn from the deck if it has one.
%% Deck is empty, don't draw a card.
update_hand(Player, #game{deck = []} = Game) -> {Player, Game};
%% Don't draw a card if the player three or more cards.
update_hand(#player{hand = Hand} = Player, Game) when length(Hand) > 2 -> {Player, Game};
%% Draw a card from the pile under normal conditions.
update_hand(Player, #game{deck = [Top|Deck]} = Game) ->
	Hand = lists:keysort(2, Player#player.hand++[Top]),
	{Player#player{hand = Hand}, Game#game{deck = Deck}}.


%% Puts a card on the pile
add_to_Pile(#game{pile = []}, Card) -> [Card];
add_to_Pile(#game{pile = [Top | Rest]}, Card) -> [Card, Top | Rest].


%% Based on the pile, determine what action to take.
%% The pile has four equal cards in a row, same as the ten.
action_to_take([#card{value = V}, #card{value = V}, #card{value = V}, #card{value = V}| _ ]) -> discard_pile;
%% Two resets the stack.
action_to_take([#card{value = 2}| _ ]) -> normal;
%% Ten is a bomb, discards the pile.
action_to_take([#card{value = 10}| _ ]) -> discard_pile;
%% Five is a wild card, that can be played on top on any card.
action_to_take([#card{value = 5}| _ ]) -> normal;
%% Card on top of a five needs to be another wildcard or lower value card.
action_to_take([Card, #card{value = 5}| _ ]) ->
	if
		Card#card.value =< 5 -> normal;
		Card#card.value > 5 -> take_pile
	end;
%% Normal action, check to see if the card place on top of another is higer value card.
action_to_take([#card{value = V1}, #card{value = V2}| _ ]) ->
	if
		V1 >= V2 -> normal;
		V1 < V2 -> take_pile
	end;
%% Card that is put on an empty pile is always allowed.
action_to_take(_) -> normal.


%% Changes a players turn. Moves the current player back.
change_turn([Player|Players]) -> Players ++ [Player].

%% Removes a card from player, based on which group is played.
remove_card_from_player(Player, Card) ->
	case which_card_group_played(Player) of
		face_down ->
			Face_Down = Player#player.face_down,
			Player#player{face_down = Face_Down -- [Card]};
		face_up ->
			Face_Up = Player#player.face_up,
			Player#player{face_up = Face_Up -- [Card]};
		hand ->
			Hand = Player#player.hand,
			Player#player{hand = Hand -- [Card]}
	end.


%% check which card group player played the card.
%% Only play face down card if hand and face up cards are empty.
which_card_group_played(#player{hand=[], face_up = [], face_down = _}) -> face_down;
%% Only plays face up card if hand is empty.
which_card_group_played(#player{hand=[], face_up = _}) -> face_up;
%% If Hand is not empty play the hand.
which_card_group_played(_) -> hand.
% --------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------
% This block concerns keeping the correct order of players after a shoot in
% --------------------------------------------------------------------------------------
%% Puts the player
fix_queue([Player | Players], _, 0) -> [Player | Players];
fix_queue([Player | Players], Pid, N) ->
	io:fwrite("In fix_queue!~n"),
	PlayerPid = Player#player.pid,
	if
		PlayerPid /= Pid -> 
			io:fwrite("In fix_queue comparing PlayerPid: ~p vs Pid:~p ~n",[PlayerPid, Pid]),
			fix_queue(Players ++ [Player], Pid, N - 1);
		PlayerPid  == Pid -> 
			io:fwrite("In fix_queue returning ~p ~n",[[Player | Players]]),
			[Player | Players]
	end.
% --------------------------------------------------------------------------------------


% --------------------------------------------------------------------------------------
% This block concerns notifying other player about game state and other important values
% --------------------------------------------------------------------------------------

%% Lets players know what cards thay got after the deal
%% This hapens before the game has begun 
notify_initial_hand([], _, _) -> ok;
notify_initial_hand([Player | Players], All_players, Deck) ->
	Others = [P || P <- All_players, P /= Player],
	io:fwrite("Player notify pid ~p~n", [Player#player.pid]),
	Player#player.pid ! {game_setup, {Player, Others}, {deck, length(Deck)}},
	notify_initial_hand(Players, All_players, Deck).

%% Lets all the players know what the value of the lowest card a player has on hand is
%% This is done to let the players know who can start the game
%% Only a player with a card of the lowest value can begin
%% In the case that multiple players have the a card with this value any of them can start
notify_lowest_value([], _) -> ok;
notify_lowest_value([Player | Players], Value) ->
	Player#player.pid ! {lowest_value, Value},
	notify_lowest_value(Players, Value).


%% Notify all players of game state.
%% The game state contains all information needed for the client to print the UI
notify_state(Game_State) ->
	notify_specific_state(Game_State#game.players, Game_State).

%% Implementation of notify_state
notify_specific_state([], _Game_State) -> ok;
notify_specific_state([Player | Players], Game_State) ->
	First_Player = hd(Game_State#game.players),
	Turn = First_Player#player.pid,
	Others = [P || P <- Game_State#game.players, P /= Player],
	Player#player.pid ! {state, {Player, Others,
		Game_State#game.move, Turn, {deck, length(Game_State#game.deck)}, pile_hd(Game_State#game.pile)}},
	notify_specific_state(Players, Game_State).

%% Helper function to take head of pile, handels empty list unlike built in function hd()
pile_hd([]) -> [];
pile_hd([Card|_]) -> Card.
% --------------------------------------------------------------------------------------



% --------------------------------------------------------------------------------------
% This block concerns finding winner and notifying other player if winner is found
% --------------------------------------------------------------------------------------

%% initiates process to find winner
has_winner(#game{players = Players}) ->
	find_winner(Players).

%% recurses through all players to find winner
find_winner([]) -> no;
find_winner([Player|Players]) ->
	case is_player_winner(Player) of
		yes -> {yes, Player};
		no -> find_winner(Players)
	end.

%% Patern mach to check if hand, face up and face down cards lists are empty
%% when These lists are empty there is a winner
is_player_winner(#player{hand = [], face_up = [], face_down = []}) -> yes;
is_player_winner(_) -> no.

%% initiates the winner broadcast
notify_winner(Player, Game_State) ->
	do_notify_winner(Game_State#game.players, Player).

%% sends message to all other users to notify that there is a winner and who it is
do_notify_winner([], _Winner) -> ok;
do_notify_winner([Player|Players], Winner) ->
	Player#player.pid ! {winner, Winner},
	do_notify_winner(Players, Winner).
% --------------------------------------------------------------------------------------

