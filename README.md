# **Idiot** (Shithead) Card Game #
___
#### Implemented in Erlang  (*`INCOMPATIBLE WITH WINDOWS`*) ####
___
## Game Rules ##

* 1 - 4 players can play the game together
* The aim of the game is to be the first player to play out all your cards
* A player must play a card with a higher or equal value than the top card in the pile, when it is his turn
* If a player doesn't have a card with a sufficient value he can take a chance, playing out the top card of the deck
* If he still doesn't win the top card, he has to take the whole pile into his hand
* At any time, if a player has a card on hand that has an equal value to the pile card, he can sneak his card in even though it isn't his turn
* 4 consecutive cards with the same value in the pile will blow up the pile

### Game Setup ###

* Each player is dealt 3 cards face-down, 3 cards face-up and finally a 3 card hand
* Initially there is swap phase where players can swap cards between their hand and their face-up cards
* When all players are done swapping their cards the game starts
* Any player with the lowest card on hand gets the first turn

### Special Cards (can be played on any card) ###

* **2** - resets the deck to the lowest number
* **5** - next player must play a card lower than 5 or a special card
* **10** - blows up the pile, the entire pile is taken out of play

**************************************

## Installation / Setup ##

* Install Erlang/OTP (17.5 or later) + Rebar
* [Rebar Github Repo](https://github.com/rebar/rebar)
* Below are instructions to set the game up locally
* To setup on a network 'localhost' can be changed to an IP address

### Server Setup ###

* Open a Terminal window as root in this folder
* Run '_erl -name server@localhost -pa ebin_'
* Enter '_application:start(shithead)._'

### Client Setup ###

* Open a Terminal window as root in this folder
* Run '_erl -sname <your_username> -pa ebin_'
* Enter '_client:play(server@localhost)._'

**************************************

## About ##

* This game was developed in a 3-week course at Reykjavík University on concurrent and distributed programming

#### Authors: ####

* Atli Freyr Einarsson
* Birkir Ólafssoon
* Halldór Sigurðsson
* Starkaður Hróbjartsson