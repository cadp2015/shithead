%% Records

%% Game holds the state of one game.
-record(game, {players, move=0, deck=[], pile=[], noplayer = 4}).

%% Holds the state of a player.
-record(player, {pid, name, hand = [], face_up = [], face_down = []}).

%% Holds the state of a card.
-record(card, {value, suit}).